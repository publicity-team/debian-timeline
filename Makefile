prefix ?= /usr/local
datadir ?= ${prefix}/share/debian-timeline
symlinks ?= 1
DIR=/srv/timeline.debian.net

all: build

INPUT    := $(wildcard data/*)
OUTPUT   := $(addsuffix .xml,$(subst data/,xml/,$(basename $(INPUT))))
INSHTML  := $(addprefix ${DESTDIR}${datadir}/,$(wildcard *.html))
INSXML   := $(addprefix ${DESTDIR}${datadir}/,$(OUTPUT))
INSMEDIA := $(addprefix ${DESTDIR}${datadir}/,$(wildcard media/debian*) media/timeline_js media/timeline_ajax media/jquery)

xml/%.xml: data/% data/%/* build.py
	@mkdir -p xml
	python3 build.py $< >$@

media/timeline_js: /usr/share/javascript/timeline_js
	ln -sf /usr/share/javascript/timeline_js $@

media/timeline_ajax: /usr/share/javascript/timeline_ajax
	ln -sf /usr/share/javascript/timeline_ajax $@

media/jquery: /usr/share/javascript/jquery
	ln -sf /usr/share/javascript/jquery $@

build: $(OUTPUT) media/timeline_js media/timeline_ajax media/jquery

${DESTDIR}${datadir}:
	install -d ${DESTDIR}${datadir}

${DESTDIR}${datadir}/%.html: %.html ${DESTDIR}${datadir}
	install -m644 -t ${DESTDIR}${datadir} $<

${DESTDIR}${datadir}/xml: ${DESTDIR}${datadir}
	install -d ${DESTDIR}${datadir}/xml

${DESTDIR}${datadir}/xml/%.xml: xml/%.xml ${DESTDIR}${datadir}/xml
	install -m644 -t ${DESTDIR}${datadir}/xml $<

${DESTDIR}${datadir}/media: ${DESTDIR}${datadir}
	install -d ${DESTDIR}${datadir}/media

${DESTDIR}${datadir}/media/%: media/% ${DESTDIR}${datadir}/media
	install -m644 -t ${DESTDIR}${datadir}/media $<

${DESTDIR}${datadir}/media/timeline_%: media/timeline_% ${DESTDIR}${datadir}/media
	rm --force --recursive ${DESTDIR}${datadir}/$<
ifeq (1,${symlinks})
	ln -sf /usr/share/javascript/$(notdir $<) ${DESTDIR}${datadir}/media
else
	cp --dereference --recursive $< ${DESTDIR}${datadir}/media
endif

${DESTDIR}${datadir}/media/jquery: media/jquery ${DESTDIR}${datadir}/media
	rm --force --recursive ${DESTDIR}${datadir}/$<
ifeq (1,${symlinks})
	ln -sf /usr/share/javascript/$(notdir $<) ${DESTDIR}${datadir}/media
else
	cp --dereference --recursive $< ${DESTDIR}${datadir}/media
endif

install: build ${DESTDIR}${datadir} ${INSHTML} ${INSXML} ${INSMEDIA}

uninstall:
	rm -rf ${DESTDIR}${datadir}

clean:
	rm -rf xml media/timeline_js media/timeline_ajax media/jquery

mpublish:
	mkdir -p $(DIR)/htdocs
	./update
	sudo -u staticsync /usr/local/bin/static-update-component timeline.debian.net
	rm -rf $(DIR)/htdocs

.PHONY: all build install clean
